/*
    This file is part of PTME.

    PTME is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    PTME is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with PTME. If not, see <https://www.gnu.org/licenses/>.
 */

package ptme;

import java.io.*;
import java.util.Iterator;
import java.util.Properties;
import javax.imageio.ImageIO;
import java.util.Vector;
import java.util.Map.Entry;
import java.awt.geom.AffineTransform;

import tiled.core.LayerLockedException;
import tiled.core.Tile;
import tiled.core.Map;
import tiled.core.TileSet;
import tiled.io.MapHelper;
import tiled.io.MapReader;
import tiled.io.PluginLogger;
import tiled.core.TileLayer;

public class PTMapReader implements MapReader {
	
	private PluginLogger logger;
	
	public Map readMap(String filename) throws Exception {
		return this.readMap(new FileInputStream(filename));
	}
	
	public Map readMap(InputStream reader) throws Exception {
		Map map = new Map(Integer.parseInt(readItem(reader, '&')), Integer.parseInt(readItem(reader, '&')));
		map.setOrientation(Map.MDO_ORTHO);
		TileSet set = MapHelper.loadTileset(readItem(reader, '&') + ".ptx");
        map.addTileset(set);
        TileLayer tiles = (TileLayer)map.getLayer(0);
        
        String[] spawns = readItem(reader, '&').split("\\^");
        
        String[] tileData = readItem(reader, '&').split("\\^");
        for (int i = 0; i < tileData.length; i++) {
        	String[] propSplit = tileData[i].split("\\$");
        	String[] props = propSplit[1].split("\\,");
        	String[] coords = propSplit[0].split("\\,");
        	
        	Tile t = new Tile(set.getTile(Integer.parseInt(props[0])));
        	double[] matrix = new double[4];
        	matrix[0] = Integer.parseInt(props[1].substring(0, 1)) - 1;
        	matrix[1] = Integer.parseInt(props[1].substring(1, 2)) - 1;
        	matrix[2] = Integer.parseInt(props[1].substring(2, 3)) - 1;
        	matrix[3] = Integer.parseInt(props[1].substring(3, 4)) - 1;
        	t.setTransform(new AffineTransform(matrix));
        	int height = Integer.parseInt(props[2]);
        	t.getProperties().put("Level", height);
        	t.getProperties().put("Special", Integer.parseInt(props[3]));
        	
        	tiles.setTileAt(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), t);
        }
        
        for (int i = 0; i < spawns.length; i++) {
        	if (spawns[i].equals("")) {
        		break;
        	}
        	tiles.getTileAt(Integer.parseInt(spawns[i].substring(spawns[i].indexOf('$') + 1,  spawns[i].indexOf(','))), Integer.parseInt(spawns[i].substring(spawns[i].indexOf(',') + 1,  spawns[i].length()))).getProperties().put("Spawn", Integer.parseInt(spawns[i].substring(0,  spawns[i].indexOf('$'))));
        }
        
        return map;
	}
	
	public TileSet readTileset(String filename) throws Exception {
		return this.readTileset(new FileInputStream(filename));
	}
	
	public TileSet readTileset(InputStream reader) throws Exception {
		TileSet tileset = new TileSet();
		
		tileset.setName(readItem(reader, '&'));
		
		while (reader.available() > 0) {
			Tile t = new Tile();
			int id = Integer.parseInt(readItem(reader, '$'));
			t.setId(id);
			t.setImage(id);
			
			Properties p = (Properties)tileset.getDefaultProperties().clone();
			
			String props = readItem(reader, '$');
			int lastToken = 0;
			String prop = null;
			for (int i = 0; i < props.length(); i++) {
				if (props.charAt(i) == ':') {
					prop = props.substring(lastToken, i);
					lastToken = i + 1;
				} else if (props.charAt(i) == ',') {
					if (p.get(prop) instanceof Integer) {
						p.put(prop, Integer.parseInt(props.substring(lastToken, i)));
					} else if (p.get(prop) instanceof Boolean) {
						p.put(prop, Boolean.parseBoolean(props.substring(lastToken, i)));
					}
					lastToken = i + 1;
				}
			}
			
			t.setImage(ImageIO.read(new ByteArrayInputStream(readPNGBytes(reader))));
			tileset.addTile(t);
			t.setProperties(p);
		}
		
		reader.close();
		
		return tileset;
	}
	
	public String readItem (InputStream reader, char delim) throws Exception {
		String item = "";
		char ch;
		while ((ch = (char)reader.read()) != delim && ch != -1) {
			item += ch;
		}
		return item;
	}
	
	public byte[] readPNGBytes (InputStream reader) throws Exception {
		byte[] png = new byte[reader.available()];
		int size = 0;
		while (size < 8 || png[size - 8] != 'I' || png[size - 7] != 'E' || png[size - 6] != 'N' || png[size - 5] != 'D') {
			png[size] = (byte)reader.read();
			size++;
		}
		return png;
	}
	
	public String getDescription() {
		return "Reads Pawn Tactics map files.";
	}
	
	public String getFilter() throws Exception {
		return "ptm";
	}
	
	public String getName() {
		return "Pawn Tactics Map";
	}
	
	public String getPluginPackage() {
		return "Pawn Tactics Reader Plugin";
	}
	
	public void setLogger(PluginLogger logger) {
		this.logger = logger;
	}

	public boolean accept(File pathname) {
		try {
			String path = pathname.getCanonicalPath();
			if (path.endsWith(".ptm")) {
				return true;
			}
		} catch (IOException e) {}
		return false;
	}

}
