/*
    This file is part of PTME.

    PTME is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    PTME is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with PTME. If not, see <https://www.gnu.org/licenses/>.
 */

package firstmate;

import java.awt.geom.AffineTransform;

import tiled.core.Tile;
import tiled.core.TileSet;
import tiled.io.MapHelper;

public class Stamp {
	private String tileset;
	private int trigger, width, height;
	private TileSet set;
	Tile stamp[][];

	public Stamp(TileSet set, String tileset, int trigger, int width, int height) {
		this.set = set;
		// Extract actual name. Comes in form: "name.ptx" => name
		this.tileset = tileset;
		this.trigger = trigger;
		this.width = width;
		this.height = height;
		stamp = new Tile[width][height];
		// Fill the storage variable with default value of 0.
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				stamp[i][j] = set.getFirstTile();
			}
		}
	}

	public int getTrigger() {
		return this.trigger;
	}

	public String getTileSet() {
		return this.tileset;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setId(int tx, int ty, int tid, double[] matrix, int level,
			int special) {
		Tile t = new Tile(set.getTile(tid));
		t.setTransform(new AffineTransform(matrix));
		t.getProperties().put("Level", level);
		t.getProperties().put("Special", special);
		stamp[tx][ty] = t;
	}

	public Tile getId(int tx, int ty) {
		return stamp[tx][ty];
	}

	public String toString() {
		String ret = "[Trigger:" + trigger + "]";
		return ret;
	}
}
